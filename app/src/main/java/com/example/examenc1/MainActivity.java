package com.example.examenc1;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

public class MainActivity extends AppCompatActivity {
    private TextView lblCalculadora, lblUsuario, lblContraseña;
    private EditText txtUsuario, txtContraseña;
    private Button btnIngresar, btnSalir;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Initialize views
        lblCalculadora = findViewById(R.id.lblCalculadora);
        lblUsuario = findViewById(R.id.lblUsuario);
        lblContraseña = findViewById(R.id.lblConraseña);
        txtUsuario = findViewById(R.id.txtUsuario);
        txtContraseña = findViewById(R.id.txtContraseña);
        btnIngresar = findViewById(R.id.btnIngresar);
        btnSalir = findViewById(R.id.btnSalir);

        btnIngresar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String usuario = txtUsuario.getText().toString();
                String contrasena = txtContraseña.getText().toString();

                if (usuario.isEmpty() || contrasena.isEmpty()) {
                    Toast.makeText(MainActivity.this, "Por favor, ingrese un usuario y contraseña", Toast.LENGTH_SHORT).show();
                } else if (usuario.equals("lobatos") && contrasena.equals("789")) {
                    Intent intent = new Intent(MainActivity.this, CalculadoraActivity.class);
                    intent.putExtra("Usuario", "Jose Lopez");
                    startActivity(intent);
                } else {
                    Toast.makeText(MainActivity.this, "Usuario o contraseña incorrectos", Toast.LENGTH_SHORT).show();
                }
            }
        });

        btnSalir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }
}
