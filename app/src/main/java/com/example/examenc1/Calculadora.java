package com.example.examenc1;

public class Calculadora {
    private float num1, num2;

    public Calculadora() {
        this.num1 = 0.0f;
        this.num2 = 0.0f;
    }

    public float suma() {
        return this.num1 + this.num2;
    }

    public float resta() {
        return this.num1 - this.num2;
    }

    public float multiplicacion() {
        return this.num1 * this.num2;
    }

    public float division() {
        if (this.num2 != 0) {
            return this.num1 / this.num2;
        } else {
            throw new ArithmeticException("División por cero no está permitida");
        }
    }

    // Métodos setter para asignar valores a num1 y num2
    public void setNum1(float num1) {
        this.num1 = num1;
    }

    public void setNum2(float num2) {
        this.num2 = num2;
    }

    // Métodos getter para obtener valores de num1 y num2
    public float getNum1() {
        return num1;
    }

    public float getNum2() {
        return num2;
    }
}
