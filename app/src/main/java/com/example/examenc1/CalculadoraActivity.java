package com.example.examenc1;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

public class CalculadoraActivity extends AppCompatActivity {
    private Calculadora calculadora;
    private TextView lblCalculadora;
    private TextView lblUsuario;
    private TextView lblNum1, lblNum2;
    private TextView lblResultado;
    private EditText txtNum1;
    private EditText txtNum2;
    private Button btnSuma, btnResta, btnMult, btnDiv;
    private Button btnSalir, btnLimpiar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calculadora);

        // Initialize views
        lblCalculadora = findViewById(R.id.lblCalculadora);
        lblUsuario = findViewById(R.id.lblUsuario);
        lblNum1 = findViewById(R.id.lblNum1);
        lblNum2 = findViewById(R.id.lblNum2);
        lblResultado = findViewById(R.id.lblResultado);
        txtNum1 = findViewById(R.id.txtNum1);
        txtNum2 = findViewById(R.id.txtNum2);
        btnSuma = findViewById(R.id.btnSuma);
        btnResta = findViewById(R.id.btnResta);
        btnMult = findViewById(R.id.btnMult);
        btnDiv = findViewById(R.id.btnDiv);
        btnSalir = findViewById(R.id.btnSalir);
        btnLimpiar = findViewById(R.id.btnLimpiar);

        calculadora = new Calculadora();

        btnSuma.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                realizarOperacion("suma");
            }
        });

        btnResta.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                realizarOperacion("resta");
            }
        });

        btnMult.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                realizarOperacion("multiplicacion");
            }
        });

        btnDiv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                realizarOperacion("division");
            }
        });

        btnLimpiar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                limpiarCampos();
            }
        });

        btnSalir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        // Get the user name from the intent and display it
        String usuario = getIntent().getStringExtra("Usuario");
        lblUsuario.setText("Usuario: " + usuario);
    }

    private void realizarOperacion(String operacion) {
        String strNum1 = txtNum1.getText().toString();
        String strNum2 = txtNum2.getText().toString();

        if (strNum1.isEmpty() || strNum2.isEmpty()) {
            Toast.makeText(CalculadoraActivity.this, "Por favor, ingrese ambos números", Toast.LENGTH_SHORT).show();
            return;
        }

        float num1 = Float.parseFloat(strNum1);
        float num2 = Float.parseFloat(strNum2);

        calculadora.setNum1(num1);
        calculadora.setNum2(num2);

        float resultado = 0.0f;

        switch (operacion) {
            case "suma":
                resultado = calculadora.suma();
                break;
            case "resta":
                resultado = calculadora.resta();
                break;
            case "multiplicacion":
                resultado = calculadora.multiplicacion();
                break;
            case "division":
                try {
                    resultado = calculadora.division();
                } catch (ArithmeticException e) {
                    Toast.makeText(CalculadoraActivity.this, e.getMessage(), Toast.LENGTH_SHORT).show();
                    return;
                }
                break;
        }

        lblResultado.setText("Resultado: " + resultado);
    }

    private void limpiarCampos() {
        txtNum1.setText("");
        txtNum2.setText("");
        lblResultado.setText("Resultado: ");
    }
}
